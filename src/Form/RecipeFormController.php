<?php

namespace Drupal\recipe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Implements RecipeFormController form controller.
 *
 * This demonstrates the different input elements that are used to
 * collect data in a form.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class RecipeFormController extends FormBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new EmailExampleGetFormPage.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager) {
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('language_manager')
    );
  }

  /**
   * Getter method for Form ID.
   *
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'recipe_form';
  }

  /**
   * Build the recipe form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // A text field with the recipe title.
    $form['recipe_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Recipe Title'),
      '#description' => t('Please enter name of recipe'),
      '#required' => TRUE,
    );

    // A text field with the author name.
    $form['author_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name of the Author'),
      '#description' => t('Please enter name of author of recipe'),
      '#required' => TRUE,
    );

    // A text field with the email address of the author.
    $form['author_email'] = array(
      '#type' => 'email',
      '#title' => t('Email Address of the Author'),
      '#description' => t('Please enter email Id of author of recipe'),
      '#required' => TRUE,
    );

    // A text area for a recipe description.
    $form['recipe_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Recipe Description'),
      '#description' => t('Please enter description of recipe'),
      '#required' => TRUE,
    );

    // A text area for recipe instructions.
    $form['recipe_instruction'] = array(
      '#type' => 'textarea',
      '#title' => t('Recipe Instruction'),
      '#description' => t('Please enter instruction of recipe'),
      '#required' => TRUE,
    );

    // A text area for recipe ingredients.
    $form['recipe_ingredients'] = array(
      '#type' => 'textarea',
      '#title' => t('Recipe Ingredients'),
      '#description' => t('Please enter ingredients of recipe'),
      '#required' => TRUE,
    );

    // Submit button.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit recipe'),
    );
    return $form;
  }

  /**
   * Validates the recipe form.
   * Though client side validations are working for form.
   * However, we should apply server side validations as well.
   *
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Sanitize all input fields by using HTML::escape($text).
    // This method escapes HTML for sanitization purposes by
    // replacing the following special characters
    // with their HTML entity equivalents

    // Also, Check all required fields are entered.
    foreach ($form as $key => $value) {
      // Check for the fields which have required attribute set to 1.
      if (isset($value['#required']) && $value['#required'] == '1') {
        $form_state->setValue($key, Html::escape($form_state->getValue($key)));
        // If those fields's values are empty
        // set form error and do not submit form.
        if ($form_state->getValue($key) == '') {
          $form_state->setErrorByName($key, t('Please enter required fields.'));
        }
      }
    }
    // Check if author's email is valid of not.
    if (!valid_email_address($form_state->getValue('author_email'))) {
      $form_state->setErrorByName('email', t('That e-mail address is not valid.'));
    }
  }

  /**
   * Implements submitForm callback.
   *
   * When user submit recipe form successfully.
   * 1. Recipe data will be stored in custom table 'recipe'.
   * 2. Email will trigger to site admin with link of recipe created.
   * 3. User will be redirected to home page .
   * 4. User will be shown with success message.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get submitted values by user.
    $recipe = $form_state->getValues();
    $title = $recipe['recipe_title'];
    $name = $recipe['author_name'];
    $email = $recipe['author_email'];
    $description = $recipe['recipe_description'];
    $instruction = $recipe['recipe_instruction'];
    $ingredients = $recipe['recipe_ingredients'];
    $timestamp = REQUEST_TIME;
    // Prepare an array of fields
    // to store in custom table 'recipe'.
    $field = array(
      'title' => $title,
      'author_name' => $name,
      'author_email' => $email,
      'description' => $description,
      'instructions' => $instruction,
      'ingredients' => $ingredients,
      'timestamp' => $timestamp,
    );
    // Insert data to custom table recipe.
    $query = \Drupal::database();
    $rid = $query ->insert('recipe')
      ->fields($field)
      ->execute();
    // Set drupal message after submission of data in table.
    $message = t('A recipe of @title is added and its unique id is @rid ', array('@title' => $title, '@rid' => $rid));
    \Drupal::logger('recipe')->notice($message);

    // Invoka function to trigger email.
    $this->sendRecipeEmail($rid);
    // Redirect user to home page.
    $form_state->setRedirect('<front>');
    // Set success message.
    drupal_set_message(t('Success: Recipe is added'));
  }

  /**
   * Send email to Admin regarding recipe addtion.
   *
   * @param int $rid
   *   Unique ID of recipe.
   */
  public function sendRecipeEmail($rid) {
    // All system mails need to specify the module and template key (mirrored
    // from hook_mail()) that the message they want to send comes from.
    $module = 'recipe';
    $key = 'recipe_submission_message';
    // Specify 'to' and 'from' addresses.
    $to = $this->config('system.site')->get('mail');
    $from = $this->config('system.site')->get('mail');
    // "params" loads in additional context for email content completion in
    // hook_mail(). In this case, we want to pass in the values the user entered
    // into the form, which include the message body in $form_values['message'].
    // $params = $rid;
    // Prepare link of recipe which needs
    // to be sent in email.
    $host = \Drupal::request()->getHost();
    $recipe_link = $host . '/recipe/' . $rid;
    // $recipe_url = Url::fromRoute('recipe.recipe', array('rid' => $rid));
    // $recipe_link= Link::fromTextAndUrl(t('View Recipe'), $recipe_url)->toRenderable();
    $params['subject'] = t('New recipe');
    $params['body'] = $recipe_link;
    // The language of the e-mail. This will one of three values:
    // - $account->getPreferredLangcode(): Used for sending mail to a particular
    //   website user, so that the mail appears in their preferred language.
    // - \Drupal::currentUser()->getPreferredLangcode(): Used when sending a
    //   mail back to the user currently viewing the site. This will send it in
    //   the language they're currently using.
    // - \Drupal::languageManager()->getDefaultLanguage()->getId: Used when
    //   sending mail to a pre-existing, 'neutral' address, such as the system
    //   e-mail address, or when you're unsure of the language preferences of
    //   the intended recipient.
    //
    // Since in our case, we are sending a message to a random e-mail address
    // that is not necessarily tied to a user account, we will use the site's
    // default language.
    $language_code = $this->languageManager->getDefaultLanguage()->getId();
    // Whether or not to automatically send the mail when we call mail() on the
    // mail manager. This defaults to TRUE, and is normally what you want unless
    // you need to do additional processing before the mail manager sends the
    // message.
    $send_now = TRUE;
    // Send the mail, and check for success. Note that this does not guarantee
    // message delivery; only that there were no PHP-related issues encountered
    // while sending.
    $result = $this->mailManager->mail($module, $key, $to, $language_code, $params, $from, $send_now);
    if ($result['result'] == TRUE) {
      $message = t('An email notification has been sent to @email ', array('@email' => $to));
      \Drupal::logger('recipe')->notice($message);
    }
    else {
      $message = t('There was a problem sending your message and it was not sent.');
      \Drupal::logger('recipe')->error($message);
    }
  }
}
