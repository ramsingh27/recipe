<?php

namespace Drupal\recipe\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class RecipeDetailController.
 */
class RecipeDetailController extends ControllerBase {

  /**
   * Displays details about a specific recipe.
   *
   * @param int $rid
   *   Unique ID of recipe.
   *
   * @return array
   *   If the ID is located in the recipe table, a build array of recipe
   *   details exposed to twig template file.
   */
  public function getRecipe($rid) {
    // Prepare query to fetch recipe details
    // and where condition based on rid.
    $result = array();
    $query = \Drupal::database()
      ->select('recipe', 'rec')
      ->fields('rec', ['rid', 'title', 'author_name', 'author_email', 'description', 'instructions', 'ingredients', 'timestamp'])
      ->condition('rid', $rid)
      ->execute();
    // Fetch result of query.
    $result = $query->fetchAssoc();
    // Convert unix format of timestamp to long format.
    if (isset($result['timestamp'])) {
      $result['timestamp'] = format_date($result['timestamp']);
    }
    // Expose recipe data to recipe_detail_page.html.twig.
    $renderable = [
      '#theme' => 'recipe_detail_page',
      '#recipe_data' => $result,
    ];
    return $renderable;
  }
}
