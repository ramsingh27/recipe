<?php

/**
 * @file
 * Install, update and uninstall functions for the recipe module.
 */

/**
 * Implements hook_schema().
 *
 * Defines the database tables used by this module.
 *
 * @see hook_schema()
 *
 * @ingroup recipe
 */
function recipe_schema() {
  $schema['recipe'] = [
    'description' => 'Stores information related to recipes.',
    'fields' => [
      'rid' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: recipe ID.',
      ],
      'title' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Title of recipe.',
      ],
      'author_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Author of recipe.',
      ],
      'author_email' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Email of author.',
      ],
      'description' => [
        'type' => 'text',
        'not null' => TRUE,
        'description' => 'Description of recipe.',
      ],
      'instructions' => [
        'type' => 'text',
        'not null' => TRUE,
        'description' => 'Instructions of recipe.',
      ],
      'ingredients' => [
        'type' => 'text',
        'not null' => TRUE,
        'description' => 'Ingredients of recipe.',
      ],
      'timestamp' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when recipe is added.',
      ],
    ],
    'primary key' => ['rid'],
  ];

  return $schema;
}
